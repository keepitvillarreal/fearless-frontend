function createCard(name, description, pictureUrl) {
  const currentDate = new Date().toLocaleDateString();
  return `
    <div class="col-md-4 mb-4">
      <div class="card shadow">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <p class="card-text">${description}</p>
          <div class="card-footer text-muted">${currentDate}</div>
        </div>
      </div>
    </div>
  `;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);


      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
            console.log(conference);
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
              const details = await detailResponse.json();
              const title = details.conference.name;
            //   console.log(title);
              const description = details.conference.description;
              const pictureUrl = details.conference.location.picture_url;
              const html = createCard(title, description, pictureUrl);
            //   console.log(html);
              const column = document.querySelector('.col');
              column.innerHTML += html;
              console.log(column)
            }
        }


        // const conference_1 = data.conferences[0];
        // console.log(conference_1)
        // const nameTag = document.querySelector('.card-title');
        // nameTag.innerHTML = conference.name;

        // const detailUrl = `http://localhost:8000${conference.href}`;
        // const detailResponse = await fetch(detailUrl);
        // if (detailResponse.ok) {
        //   const details = await detailResponse.json();
        //   const description = details.conference.description;
        //   const descriptionTag = document.querySelector('.card-text');
        //   descriptionTag.innerHTML= description;

        //   const imageTag = document.querySelector('.card-img-top');
        //   imageTag.src =details.conference.location.picture_url;
        //   console.log(details);
        // }

      }
    } catch (e) {
      // Figure out what to do if an error is raised
    }

  });
